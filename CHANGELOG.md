# Changelog

## 0.4.0

* a379eab (HEAD -> master) Bump to version 0.4.0
* 1a649db (origin/master) Merge branch '6-add-auto-release-mode' into 'master'
* 96c3ddb (origin/6-add-auto-release-mode, 6-add-auto-release-mode) remove pipeline on master, set on tag ref
* 4415f30 add release step to create new release issues #6
* 0cd5086 Merge branch '7-remove-test-for-version' into 'master'
* 0278b17 Merge branch '5-remove-documentation-build-process-on-merge-request' into 'master'
* 5be94b9 (origin/7-remove-test-for-version, 7-remove-test-for-version) Remove useless version pytest issues #7
* abb2209 (origin/5-remove-documentation-build-process-on-merge-request, 5-remove-documentation-build-process-on-merge-request) Remove build documentation on merge request CI issues #5

## 0.3.1

* ccefa32 (tag: 0.3.1) upgrade version to 0.3.1 to keep stability with Pyp

## 0.3.0

* 9dcda28 (HEAD -> master, tag: 0.3.0, origin/master) release 0.3.0
* 5faf2e7 Merge branch '4-compute-number-and-string' into 'master'
* e053d5e (origin/4-compute-number-and-string, 4-compute-number-and-string) add compute string function, add some new tests and complete documentation
* a98b57e (origin/5-remove-documentation-build-process-on-merge-request) Add gitlab-ci to each merge request
* ce3c193 Merge branch '3-add-test-with-python-3-8' into 'master'
* cd28b1a (origin/3-add-test-with-python-3-8, 3-add-test-with-python-3-8) add new env for tox, test python 3.7 and python 3.8
* cd93302 Update README.md and docs
* 97d4ddf add some badge, link with pypi
* 5232213 add new test, 100% coverage
* f0f193a update CHANGELOG.md, remove old lib

## 0.2.0

* bfa28d6 (HEAD -> master, tag: 0.2.0, origin/master) commit
* bbf6594 add tools.py
* 7247c9d add simple function
* 6d73c35 fix tox
* bc882b5 fix tox
* 5fc1544 Merge branch '2-add-coverage' into 'master'
* fcc53bd work in progress coverage
* ef09cb5 fix README.md
* ed11bce work in progress badge
* d5b8225 work in progress badge
* b679b06 work in progress badge
* 69dde98 work in progress to fix badge
* cb11d20 work in progress to fix badge
* 5adf241 commit fix wrong path to badge
* a7714ee Merge branch '1-add-badge' into 'master'
* 0ac55ea work in progress test badge
* 0dfc31c fix typo

## 0.1.0

* 39c5e66 (tag: 0.1.0) work in progress gitlab-ci force to only master
* 4a7fe82 work in progess gitlab-ci
* ef83b25 work in progess gitlab-ci
* 3762e16 work in progess gitlab-ci
* 9c125ef fix gitlab ci with pages keyword
* 7f2b036 gitlab ci add buildoc and cache
* 05c1cc6 fix wrong version of python
* 99f9a5e work in progress .gitlab-ci
* 710e66f force to python 3.7
* a832904 add .gitlab-ci, remove building docs
* d6b5d94 add documentation
* 5807b42 adding changelog