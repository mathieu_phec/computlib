# Computlib

A simple compute library

[![PyPI](https://img.shields.io/pypi/v/computlib)](https://pypi.org/project/computlib/)
[![PyPI - License](https://img.shields.io/pypi/l/computlib)](https://pypi.org/project/computlib/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/computlib)](https://pypi.org/project/computlib/)
[![pipeline status](https://gitlab.com/mlysakowski/computlib/badges/master/pipeline.svg)](https://gitlab.com/mlysakowski/computlib/-/commits/master)
[![coverage report](https://gitlab.com/mlysakowski/computlib/badges/master/coverage.svg)](https://gitlab.com/mlysakowski/computlib/-/commits/master)
[![python:3.7](https://gitlab.com/mlysakowski/computlib/-/jobs/artifacts/master/raw/test_python3.7.svg?job=python3.7)](https://gitlab.com/mlysakowski/computlib/-/commits/master)
[![python:3.8](https://gitlab.com/mlysakowski/computlib/-/jobs/artifacts/master/raw/test_python3.8.svg?job=python3.8)](https://gitlab.com/mlysakowski/computlib/-/commits/master)
[![bandit](https://gitlab.com/mlysakowski/computlib/-/jobs/artifacts/master/raw/test_bandit.svg?job=check-bandit)](https://gitlab.com/mlysakowski/computlib/-/commits/master)
[![black](https://gitlab.com/mlysakowski/computlib/-/jobs/artifacts/master/raw/test_black.svg?job=check-black)](https://gitlab.com/mlysakowski/computlib/-/commits/master)
[![safety](https://gitlab.com/mlysakowski/computlib/-/jobs/artifacts/master/raw/test_safety.svg?job=check-safety)](https://gitlab.com/mlysakowski/computlib/-/commits/master)
[![flake8](https://gitlab.com/mlysakowski/computlib/-/jobs/artifacts/master/raw/test_flake8.svg?job=check-flake8)](https://gitlab.com/mlysakowski/computlib/-/commits/master)
[![documentation](https://gitlab.com/mlysakowski/computlib/-/jobs/artifacts/master/raw/documentation.svg?job=pages)](https://gitlab.com/mlysakowski/computlib/-/commits/master)


## Documentations

See [Documentation page](https://mlysakowski.gitlab.io/computlib/)
