from .main import compute
from .tools import getVersion

__version__ = getVersion()
