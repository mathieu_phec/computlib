# Basic Usage

This is a simple usage of computlib :

```python
>>> import computlib
>>> computlib.compute(1,2)
3
```

You can use many args to compute.

## Type of compute

You can compute actually two types :
* Integer (int)
* String (str)

### Compute Integer

This is the default compute.

For example, to compute 1,2,3, use :

```python
>>> import computlib
>>> computlib.compute(1, 2, 3)
6
```

### Compute String

You can compute string.

For example, to compute "a", "b" use :

```python
>>> import computlib
>>> computlib.compute("a", "b")
"ab"
```

It's possible to reverse string computation.

```python
>>> import computlib
>>> computlib.compute("a", "b", reverse=True)
"ba"
```