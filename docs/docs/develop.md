# How to develop

## Requierements

* [Poetry](https://python-poetry.org/)

## Setup your env

* To setup your env, run `poetry shell`

## Test your code

* To test your code, run `poetry run tox` (after `poetry shell`)

### Additionnal test

#### Bandit

* To test your code with [Bandit](https://bandit.readthedocs.io/en/latest/), run `poetry run tox -e bandit` (after `poetry shell`)

#### Safety

* To test your code with [Safety](https://github.com/pyupio/safety), run `poetry run tox -e safety` (after `poetry shell`)

#### Black

* To lint your code with [Black](https://github.com/psf/black), run `poetry run tox -e black` (after `poetry shell`)

#### Coverage

* To test your code with [Pytest-cov](https://github.com/pytest-dev/pytest-cov), run `poetry run tox -e coverage` (after `poetry shell`)

## Get commit for changelog

* To get log commit between two tags : `git log "0.1.0".."0.2.0" --oneline`

