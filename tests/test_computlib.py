import pytest
import computlib


def test_no_pyprojectoml():
    """
        Test if no pyproject.toml exists
        Raise Error
    """
    with pytest.raises(Exception):
        computlib.getVersion("dump/pyproject.toml")


def test_compute():
    """
        Compute simple value
    """
    assert computlib.compute(1, 2) == 3


def test_compute_many():
    """
        Compute many value
    """
    assert computlib.compute(1, 2, 3, 4, 5) == 15


def test_compute_string():
    """
        Compute string value
    """
    assert computlib.compute("a", "b") == "ab"


def test_compute_multiple_type():
    """
        Compute with string and number value
        Raise Error
    """
    with pytest.raises(Exception):
        computlib.compute("a", 1)


def test_compute_string_reverse():
    """
        Compute string value with reverse
    """
    assert computlib.compute("a", "b", reverse=True) == "ba"
